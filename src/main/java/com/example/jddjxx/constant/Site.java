package com.example.jddjxx.constant;

import java.util.List;
import java.util.stream.Collectors;

public class Site {
//        private SiteScheme price;
//        private SiteScheme goods;
//        private SiteScheme afs;
//        private SiteScheme sale;
//        private SiteScheme store;
//        private SiteScheme vip;
//        private SiteScheme money;
//        private SiteScheme easy;
//        private SiteScheme order1;
//        private SiteScheme order2;
//        private SiteScheme order3;
//        private SiteScheme order4;
//        private List<SiteScheme> ss;

    private String scheme;
    private String keyword;
    private boolean subscribe;
    private List<SiteMsg> msgs;

//        private static class SiteScheme{
//            private String keyword;
//            private boolean subscribe;
//            private List<SiteMsg> msgs;
//        }


    public String getScheme() {
        return scheme;
    }

    public void setScheme(String scheme) {
        this.scheme = scheme;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public boolean isSubscribe() {
        return subscribe;
    }

    public void setSubscribe(boolean subscribe) {
        this.subscribe = subscribe;
    }

    public List<SiteMsg> getMsgs() {
        return msgs;
    }

    public void setMsgs(List<SiteMsg> msgs) {
        this.msgs = msgs;
    }

    public static class SiteMsg {
        private String key;
        private String value;
        private boolean checked;

        public SiteMsg() {
        }

        public SiteMsg(String key, String value) {
            this.key = key;
            this.value = value;
        }

        public SiteMsg(Msg msg) {
            this.key = msg.name();
            this.value = msg.getTitle();
            this.checked = msg.getDefautChecked();
        }

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public boolean isChecked() {
            return checked;
        }

        public void setChecked(boolean checked) {
            this.checked = checked;
        }
    }

    public Site() {
    }

    public Site(String scheme, String keyword, boolean subscribe, List<SiteMsg> msgs) {
        this.scheme = scheme;
        this.keyword = keyword;
        this.subscribe = subscribe;
        this.msgs = msgs;
    }

    public Site(Scheme scheme) {
        this.scheme = scheme.name();
        this.keyword = scheme.keyword();
        this.subscribe = scheme.msg();
        final List<SiteMsg> collect = scheme.msgs().stream()
                .map(SiteMsg::new)
                .collect(Collectors.toList());
        this.msgs = collect;
    }


}
