package com.example.jddjxx.constant;

import lombok.Getter;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * <p>Msg</p>
 * description
 *
 * @author 邓峰峰
 * @date 2020/5/11 17:38
 */
@Getter
public enum Msg {


    newAfterSaleBill("创建售后单消息"){
        @Override
        public List<Scheme> schemes() {
            return Arrays.asList(Scheme.order3,Scheme.price,Scheme.afs);
        }

        @Override
        public boolean getDefautChecked() {
            return true;
        }
    },

    afterSaleBillStatus("售后单状态消息"){
        @Override
        public List<Scheme> schemes() {
            return Arrays.asList(Scheme.order3,Scheme.price,Scheme.afs);
        }

        @Override
        public boolean getDefautChecked() {
            return true;
        }
    },

    newApplyAfterSaleBill("创建售后单申请消息"){
        @Override
        public List<Scheme> schemes() {
            return Arrays.asList(Scheme.order3,Scheme.price,Scheme.afs);
        }

        @Override
        public boolean getDefautChecked() {
            return true;
        }
    },

    updateApplyAfterSaleBill("修改售后单申请消息"){
        @Override
        public List<Scheme> schemes() {
            return Arrays.asList(Scheme.order3,Scheme.price,Scheme.afs);
        }

        @Override
        public boolean getDefautChecked() {
            return true;
        }
    },

    afterSaleJdBillFinish("售后单最终完成消息（物竞天择）"){
        @Override
        public List<Scheme> schemes() {
            return Arrays.asList(Scheme.afs);
        }

        @Override
        public boolean getDefautChecked() {
            return true;
        }
    },


    newSku("新增商品消息"){
        @Override
        public List<Scheme> schemes() {
            return Arrays.asList(Scheme.goods);
        }

        @Override
        public boolean getDefautChecked() {
            return true;
        }
    },

    updateSku("修改商品消息"){
        @Override
        public List<Scheme> schemes() {
            return Arrays.asList(Scheme.goods);
        }

        @Override
        public boolean getDefautChecked() {
            return false;
        }
    },

    stockIsHave("门店商品库存上下架状态消息"){
        @Override
        public List<Scheme> schemes() {
            return Arrays.asList(Scheme.goods,Scheme.price);
        }

        @Override
        public boolean getDefautChecked() {
            return true;
        }
    },

    brandCreated("新增商品品牌消息"){
        @Override
        public List<Scheme> schemes() {
            return Arrays.asList(Scheme.goods);
        }

        @Override
        public boolean getDefautChecked() {
            return false;
        }
    },

    brandModify("修改商品品牌消息"){
        @Override
        public List<Scheme> schemes() {
            return Arrays.asList(Scheme.goods);
        }

        @Override
        public boolean getDefautChecked() {
            return false;
        }
    },

    categoryInfoOper("新增或修改商品类目消息"){
        @Override
        public List<Scheme> schemes() {
            return Arrays.asList(Scheme.goods);
        }

        @Override
        public boolean getDefautChecked() {
            return false;
        }
    },

    uploadSkuPictureFail("商品图片上传失败消息"){
        @Override
        public List<Scheme> schemes() {
            return Arrays.asList(Scheme.goods);
        }

        @Override
        public boolean getDefautChecked() {
            return false;
        }
    },


    orderAccounting("订单应结消息"){
        @Override
        public List<Scheme> schemes() {
            return Arrays.asList(Scheme.order3,Scheme.money);
        }

        @Override
        public boolean getDefautChecked() {
            return false;
        }
    },

    financeAdjustment("财务调整单消息"){
        @Override
        public List<Scheme> schemes() {
            return Arrays.asList(Scheme.money);
        }

        @Override
        public boolean getDefautChecked() {
            return false;
        }
    },



    pickFinishOrder("拣货完成消息"){
        @Override
        public List<Scheme> schemes() {
            return Arrays.asList(Scheme.order3);
        }

        @Override
        public boolean getDefautChecked() {
            return true;
        }
    },

    deliveryOrder("订单开始配送消息"){
        @Override
        public List<Scheme> schemes() {
            return Arrays.asList(Scheme.order3);
        }

        @Override
        public boolean getDefautChecked() {
            return true;
        }
    },

    finishOrder("订单妥投消息"){
        @Override
        public List<Scheme> schemes() {
            return Arrays.asList(Scheme.order3);
        }

        @Override
        public boolean getDefautChecked() {
            return true;
        }
    },

    lockOrder("订单锁定消息"){
        @Override
        public List<Scheme> schemes() {
            return Arrays.asList(Scheme.order3);
        }

        @Override
        public boolean getDefautChecked() {
            return true;
        }
    },

    userCancelOrder("用户取消消息"){
        @Override
        public List<Scheme> schemes() {
            return Arrays.asList(Scheme.order3);
        }

        @Override
        public boolean getDefautChecked() {
            return true;
        }
    },

    newOrder("创建新订单消息"){
        @Override
        public List<Scheme> schemes() {
            return Arrays.asList(Scheme.order3);
        }

        @Override
        public boolean getDefautChecked() {
            return true;
        }
    },

    applyCancelOrder("用户申请取消消息"){
        @Override
        public List<Scheme> schemes() {
            return Arrays.asList(Scheme.order3);
        }

        @Override
        public boolean getDefautChecked() {
            return true;
        }
    },

    endOrderFinance("订单金额拆分完成消息"){
        @Override
        public List<Scheme> schemes() {
            return Arrays.asList(Scheme.order3);
        }

        @Override
        public boolean getDefautChecked() {
            return true;
        }
    },

    orderAdjust("订单调整消息"){
        @Override
        public List<Scheme> schemes() {
            return Arrays.asList(Scheme.order3);
        }

        @Override
        public boolean getDefautChecked() {
            return true;
        }
    },

    pushDeliveryStatus("订单运单状态消息"){
        @Override
        public List<Scheme> schemes() {
            return Arrays.asList(Scheme.order3);
        }

        @Override
        public boolean getDefautChecked() {
            return true;
        }
    },

    orderInfoChange("订单信息变更消息"){
        @Override
        public List<Scheme> schemes() {
            return Arrays.asList(Scheme.order3);
        }

        @Override
        public boolean getDefautChecked() {
            return true;
        }
    },

    orderWaitOutStore("订单等待出库消息"){
        @Override
        public List<Scheme> schemes() {
            return Arrays.asList(Scheme.order3);
        }

        @Override
        public boolean getDefautChecked() {
            return true;
        }
    },

    orderAddTips("订单商家小费消息"){
        @Override
        public List<Scheme> schemes() {
            return Arrays.asList(Scheme.order3);
        }

        @Override
        public boolean getDefautChecked() {
            return false;
        }
    },

    unlockOrder("订单解锁消息"){
        @Override
        public List<Scheme> schemes() {
            return Arrays.asList(Scheme.order3);
        }

        @Override
        public boolean getDefautChecked() {
            return true;
        }
    },

    deliveryCarrierModify("订单转自送消息"){
        @Override
        public List<Scheme> schemes() {
            return Arrays.asList(Scheme.order3);
        }

        @Override
        public boolean getDefautChecked() {
            return false;
        }
    },


    venderAuditApplyCancelOrder("商家审核用户取消申请消息"){
        @Override
        public List<Scheme> schemes() {
            return Arrays.asList(Scheme.order3);
        }

        @Override
        public boolean getDefautChecked() {
            return true;
        }
    },




    internetHospitalAudit("互联网医院审核处方单消息"){
        @Override
        public List<Scheme> schemes() {
            return Arrays.asList(Scheme.order3);
        }

        @Override
        public boolean getDefautChecked() {
            return false;
        }
    },

    newOrderForPrescription("创建新处方药订单消息"){
        @Override
        public List<Scheme> schemes() {
            return Arrays.asList(Scheme.order3);
        }

        @Override
        public boolean getDefautChecked() {
            return false;
        }
    },

    orderInvoice("订单发票消息"){
        @Override
        public List<Scheme> schemes() {
            return Arrays.asList(Scheme.order3);
        }

        @Override
        public boolean getDefautChecked() {
            return false;
        }
    },







    wmsMerchantStockChang("哥伦布WMS仓储库存变更消息"){
        @Override
        public List<Scheme> schemes() {
            return Arrays.asList(Scheme.price);
        }

        @Override
        public boolean getDefautChecked() {
            return false;
        }
    },



    singlePromoteCreate("单品级促销创建活动消息"){
        @Override
        public List<Scheme> schemes() {
            return Arrays.asList(Scheme.sale);
        }

        @Override
        public boolean getDefautChecked() {
            return true;
        }
    },

    /**
     * 订单级促销活动状态消息
     * 由于京东到家开放平台的bug,这里必须写"订单级促销创建活动消息"才能正常工作
     */
    orderPromoteCreate("订单级促销创建活动消息"){
        @Override
        public List<Scheme> schemes() {
            return Arrays.asList(Scheme.sale);
        }

        @Override
        public boolean getDefautChecked() {
            return true;
        }
    },

    createOrderPromote("订单级促销创建活动消息"){
        @Override
        public List<Scheme> schemes() {
            return Arrays.asList(Scheme.sale);
        }

        @Override
        public boolean getDefautChecked() {
            return false;
        }
    },

    merchantApproval("到家产生优惠券id时通知商家审批消息"){
        @Override
        public List<Scheme> schemes() {
            return Arrays.asList(Scheme.sale);
        }

        @Override
        public boolean getDefautChecked() {
            return false;
        }
    },



    orderCommentPush("新增或修改门店订单评价消息"){
        @Override
        public List<Scheme> schemes() {
            return Arrays.asList(Scheme.store);
        }

        @Override
        public boolean getDefautChecked() {
            return true;
        }
    },

    orgCommentAudit("商家评价审核完成消息"){
        @Override
        public List<Scheme> schemes() {
            return Arrays.asList(Scheme.store);
        }

        @Override
        public boolean getDefautChecked() {
            return true;
        }
    },

    storeCrud("新增或修改门店消息"){
        @Override
        public List<Scheme> schemes() {
            return Arrays.asList(Scheme.store);
        }

        @Override
        public boolean getDefautChecked() {
            return false;
        }
    },

    queryMerchantMemberInfo("查询商家会员信息接口"){
        @Override
        public List<Scheme> schemes() {
            return Arrays.asList(Scheme.store,Scheme.vip);
        }

        @Override
        public boolean getDefautChecked() {
            return false;
        }
    },


    memberCreateCard("商家会员买卡成功消息"){
        @Override
        public List<Scheme> schemes() {
            return Arrays.asList(Scheme.vip);
        }

        @Override
        public boolean getDefautChecked() {
            return false;
        }
    },

    memberRenewCard("商家会员续费成功消息"){
        @Override
        public List<Scheme> schemes() {
            return Arrays.asList(Scheme.vip);
        }

        @Override
        public boolean getDefautChecked() {
            return false;
        }
    },


    orderPaymentSuccess("超级会员码订单支付成功消息"){
        @Override
        public List<Scheme> schemes() {
            return Arrays.asList(Scheme.vip);
        }

        @Override
        public boolean getDefautChecked() {
            return false;
        }
    },

    offlineVipPointsChange("线下积分会员消息"){
        @Override
        public List<Scheme> schemes() {
            return Arrays.asList(Scheme.vip);
        }

        @Override
        public boolean getDefautChecked() {
            return false;
        }
    },


    ;

    private final String title;

    /**
     * 消息对应的方案集合
     * @return
     */
    public abstract List<Scheme> schemes();

//    private final Scheme scheme;

    Msg(String title) {
        this.title = title;
    }

//    Msg(String title, Scheme scheme) {
//        this.title = title;
//        this.scheme = scheme;
//    }

    public static final String SEPARATOR = ";&";
    public static Map<String,Msg> map(){
        return Arrays.stream(Msg.values()).collect(Collectors.toMap(
                msg -> msg.name()+ SEPARATOR +msg.title,
                Function.identity(),
                (a,b)->a
        ));
    }

    public static Msg valueBy(String title){
        for (Msg msg : Msg.values()) {
            if (msg.getTitle().equals(title)){
                return msg;
            }
        }
        return null;
    }

    public abstract boolean getDefautChecked();
}
