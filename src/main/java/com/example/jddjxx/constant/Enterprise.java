package com.example.jddjxx.constant;

import lombok.Getter;

/**
 * @author DFF
 */
@Getter
public enum Enterprise implements TestEnterprise {
    qian_niu_hua_ti_yan_dian("牵牛花体验店（cq05）","299528"){
        @Override
        public boolean test() {
            return true;
        }
    },
    jing_ke_long_chao_shi_mai_chang_fuwu("京客隆超市卖场服务","312194"),
    jing_ke_long_bian_li_dian("京客隆便利店","312195"),
    xing_fu_xing_jing_dong_dao_jia_duijie("兴福兴京东到家对接","312468"),
    guan_chao_shi_fujian("冠超市福建","308012"),
    chao_shi_fa_jing_dong_daojia("超市发京东到家","317555"),
    jing_ke_long_lang_fang_jing_dong_daojia("京客隆廊坊京东到家","317627"),
    zheng_da_you_xian_shang_hai_jing_dong_daojia("正大优鲜上海京东到家","308211"),
    jing_ke_long_bian_li_dian2("京客隆便利店2","319398"),
    ao_zhi_xing("澳之星","73423"),
    lang_fang_yuan_chen_chao_shi("廊坊元辰超市","319109"),
    da_tonghua("大统华","322049"),
    zheng_da_you_xian_cheng_du("正大优鲜—成都","313547"),
    zheng_da_you_xian_wuhan("正大优鲜武汉","313909"),
    qing_dao_lida("青岛丽达","321931"),
    guan_chao_shi_guangxi("冠超市广西","325548"),
    chong_qing_jia_jia_chao_shi("重庆家佳超市","74840"),
    shang_hai_bu_feng_lianhua("上海卜蜂莲花","316370"),
    yin_zuo_shangcheng("银座商城","326559"),
    cheng_xiang_chao_shi("城乡超市","327901"),
    zheng_da_you_xian_zheng_zhou("正大优鲜（郑州）","314214"),
    su_zhou_yi_jiale("苏州怡家乐","329808"),
    ha_er_bin_wei_li_chao_shi("哈尔滨微利超市","330995"),
    shang_hai_jia_siduo("上海佳思多","325946"),
    jin_kai_da("金凯达","339096"),
    baifenbai("百分百","343804"),
    qian_niu_hua_zheng_shi_huan_jing_("牵牛花正式环境（1129）","345344"){
        @Override
        public boolean test() {
            return true;
        }
    },
    qian_niu_hua_ce_shi_huan_jing_ji_fang("牵牛花测试环境（机房）","345345"){
        @Override
        public boolean test() {
            return true;
        }
    },
    zhang_jia_kou_chao_shi_fa("张家口超市发","345439"),
    jianfu("见福","346469"),
    jiu_jiang_liansheng("九江联盛","345570"),
    shan_bian_dachu("闪变大厨","342002"),
    bei_guo_chao_shi("北国超市","322625"),
    bao_ding_jie_chu("保定杰出","347825"),
    zheng_zhou_mei_yitian("郑州每一天","349029"),
    shan_xi_ling_zheng("山西岭峥","328214"),
    bi_gui_yuan_feng_huang_youxuan("碧桂园凤凰优选","353086"),
    liang_yun_ou_bao("良运欧宝","336987"),
    shi_jia_zhuang_xiang_tai_long("石家庄祥隆泰","323354"),
    xing_long_guang_yuan("兴隆广源","354972"),
    lang_fang_ming_zhu_chao_shi("廊坊明珠超市","355473"),
    jianfu_chengdu_quanshi("见福-成都-全时","350084"),
    guanchaoshi_yunnan("冠超市云南","358167"),
    sanmaochaoshi("三毛超市","357192"),

    ;

    private final String chinese;
    private final String venderId;







    Enterprise(String chinese, String venderId) {
        this.chinese = chinese;
        this.venderId = venderId;
    }

    public static Enterprise valueBy(String venderId){
        for (Enterprise enterprise : Enterprise.values()) {
            if (enterprise.getVenderId().equals(venderId)){
                return enterprise;
            }
        }
        return null;
    }

    @Override
    public boolean test() {
        return false;
    }
}
