package com.example.jddjxx.constant;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.example.jddjxx.constant.Msg.*;


/**
 * <p>Scheme</p>
 * description
 *
 * @author 邓峰峰
 * @date 2020/5/9 13:04
 */
public enum Scheme {
    /**
     * 价格库存方案
     */
    price {
        @Override
        public String keyword() {
            return "价格库存";
        }

        @Override
        public boolean msg() {
            return true;
        }

        @Override
        public List<Msg> msgs() {
            return Arrays.asList(
                    newAfterSaleBill,
                    afterSaleBillStatus,
                    newApplyAfterSaleBill,
                    updateApplyAfterSaleBill,
                    stockIsHave,
                    wmsMerchantStockChang
            );
        }
    },
    /**
     * 商品方案
     */
    goods {
        @Override
        public String keyword() {
            return "商品";
        }


        @Override
        public boolean msg() {
            return true;
        }

        @Override
        public List<Msg> msgs() {
            return Arrays.asList(
                    newSku,

                    updateSku,

                    stockIsHave,

                    brandCreated,

                    brandModify,

                    categoryInfoOper,

                    uploadSkuPictureFail
            );
        }
    },
    /**
     * 售后方案
     */
    afs {
        @Override
        public String keyword() {
            return "售后";
        }


        @Override
        public boolean msg() {
            return true;
        }

        @Override
        public List<Msg> msgs() {
            return Arrays.asList(
                    newAfterSaleBill,

                    afterSaleBillStatus,

                    newApplyAfterSaleBill,

                    updateApplyAfterSaleBill,

                    afterSaleJdBillFinish
            );
        }
    },
    /**
     * 促销方案
     */
    sale {
        @Override
        public String keyword() {
            return "促销";
        }


        @Override
        public boolean msg() {
            return true;
        }

        @Override
        public List<Msg> msgs() {
            return Arrays.asList(
                    singlePromoteCreate,

                    orderPromoteCreate,

                    createOrderPromote,

                    merchantApproval
            );
        }
    },
    /**
     * 门店方案
     */
    store {
        @Override
        public String keyword() {
            return "门店";
        }


        @Override
        public boolean msg() {
            return true;
        }

        @Override
        public List<Msg> msgs() {
            return Arrays.asList(

                    orderCommentPush,

                    orgCommentAudit,

                    storeCrud,

                    queryMerchantMemberInfo
            );
        }
    },
    /**
     * 会员方案
     */
    vip {
        @Override
        public String keyword() {
            return "会员";
        }


        @Override
        public boolean msg() {
            return true;
        }

        @Override
        public List<Msg> msgs() {
            return Arrays.asList(
                    memberCreateCard,

                    memberRenewCard,

                    queryMerchantMemberInfo,

                    orderPaymentSuccess,

                    offlineVipPointsChange
            );

        }
    },
    /**
     * 财务方案
     */
    money {
        @Override
        public String keyword() {
            return "财务";
        }


        @Override
        public boolean msg() {
            return true;
        }

        @Override
        public List<Msg> msgs() {
            return Arrays.asList(

                    orderAccounting,

                    financeAdjustment
            );

        }
    },
    /**
     * 轻松购方案
     */
    easy {
        @Override
        public String keyword() {
            return "轻松购";
        }


        @Override
        public boolean msg() {
            return false;
        }

        @Override
        public List<Msg> msgs() {
            return null;
        }
    },
    /**
     * 订单方案1
     */
    order1 {
        @Override
        public String keyword() {
            return "订单方案一";
        }


        @Override
        public boolean msg() {
            return false;
        }

        @Override
        public List<Msg> msgs() {
            return null;
        }
    },
    /**
     * 订单方案2
     */
    order2 {
        @Override
        public String keyword() {
            return "订单方案二";
        }

        @Override
        public boolean msg() {
            return false;
        }

        @Override
        public List<Msg> msgs() {
            return null;
        }
    },
    /**
     * 订单方案3
     */
    order3 {
        @Override
        public String keyword() {
            return "订单方案三";
        }


        @Override
        public boolean msg() {
            return true;
        }

        @Override
        public List<Msg> msgs() {
            return Arrays.asList(

                    pickFinishOrder,

                    deliveryOrder,

                    finishOrder,

                    lockOrder,

                    userCancelOrder,

                    newOrder,

                    applyCancelOrder,

                    endOrderFinance,

                    orderAdjust,

                    pushDeliveryStatus,

                    orderInfoChange,

                    orderWaitOutStore,

                    orderAddTips,

                    unlockOrder,

                    deliveryCarrierModify,

                    orderAccounting,

                    venderAuditApplyCancelOrder,

                    newAfterSaleBill,

                    afterSaleBillStatus,

                    newApplyAfterSaleBill,

                    updateApplyAfterSaleBill,

                    internetHospitalAudit,

                    newOrderForPrescription,

                    orderInvoice
            );
        }
    },
    /**
     * 订单方案4
     */
    order4 {
        @Override
        public String keyword() {
            return "订单方案四";
        }

        @Override
        public boolean msg() {
            return false;
        }

        @Override
        public List<Msg> msgs() {
            return null;
        }
    },
    ;

    /**
     * div h2 关键字
     *
     * @return
     */
    public abstract String keyword();

    /**
     * 是否需要订阅消息
     *
     * @return
     */
    public abstract boolean msg();

    public abstract List<Msg> msgs();

//    public abstract Map<String,String> allMsgMap();

    public static Scheme findByKeyword(String keyword) {
        for (Scheme scheme : Scheme.values()) {
            final boolean contains = keyword.contains(scheme.keyword());
            if (contains) {
                return scheme;
            }
        }
        throw new IllegalArgumentException("不存在Scheme");
    }

    public static void main(String[] args) throws JsonProcessingException {

        List<Site> list = new ArrayList<>();
        for (Scheme value : Scheme.values()) {
            final String key = value.name();
            final String keyword = value.keyword();

            final boolean msg = value.msg();
            List<Msg> msgs = value.msgs();
            msgs = msgs==null?new ArrayList<>():msgs;

            final Site site = new Site();
            site.setScheme(key);
            site.setKeyword(keyword);
            site.setSubscribe(msg);

            final List<Site.SiteMsg> collect = msgs.stream()
                    .map(Site.SiteMsg::new)
                    .collect(Collectors.toList());
            site.setMsgs(collect);
            list.add(site);
        }

        ObjectMapper om = new ObjectMapper();
        final String s = om.writeValueAsString(list);
        System.out.println(s);
    }


}
