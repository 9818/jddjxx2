package com.example.jddjxx.constant;

/**
 * <p>TestEnterprise</p>
 * description
 *
 * @author 邓峰峰
 * @date 2020/5/14 11:49
 */
public interface TestEnterprise {

    /**
     * 是否是测试企业
     * @return
     */
    boolean test();
}
