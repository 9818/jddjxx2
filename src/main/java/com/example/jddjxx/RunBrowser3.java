package com.example.jddjxx;

import com.example.jddjxx.tool.BareBonesBrowserLaunch;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * <p>RunBrowser</p>
 * description
 *
 * @author 邓峰峰
 * @date 8/18/2020 8:09 PM
 */
@Component
public class RunBrowser3 {



    @Value("${server.port}")
    private int port;


    @PostConstruct
    public void runBrowser(){
        BareBonesBrowserLaunch.openURL("http://localhost:"+port);
    }
}
