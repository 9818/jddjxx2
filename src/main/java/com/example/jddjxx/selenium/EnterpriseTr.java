package com.example.jddjxx.selenium;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

/**
 * <p>EnterpriseTr</p>
 * description
 *
 * @author 邓峰峰
 * @date 2020/5/12 11:39
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class EnterpriseTr extends SelfElement {
    private WebElement appName;
    private WebElement appType;
    private WebElement venderName;
    private WebElement venderId;
    private WebElement authedTime;
    private WebElement createTime;
    private WebElement scheme;
    private WebElement edit;
    private WebElement delete;

    public EnterpriseTr() {
    }

    public EnterpriseTr(WebElement self) {
        super.self = self;
        initAppName();
        initAppType();
        initVenderName();
        initVenderId();
        initAuthedTime();
        initCreateTime();

        intiOp();

    }

    @Override
    public void checked() {

    }

    @Override
    public void unchecked() {

    }

    @Override
    public void activate() {

    }

    @Override
    public void deactivate() {

    }

    private void intiOp(){
        final WebElement td = self.findElements(By.tagName("td")).get(6);
        final List<WebElement> as = td.findElements(By.tagName("a"));
        final WebElement scheme = as.get(0);
        this.scheme = scheme;
        final WebElement edit = as.get(1);
        this.edit = edit;
        final WebElement delete = as.get(2);
        this.delete = delete;
    }


    private void initAppName(){
        this.appName = self.findElements(By.tagName("td")).get(0);
    }

    private void initAppType(){
        this.appType = self.findElements(By.tagName("td")).get(1);
    }

    private void initVenderName(){
        this.venderName = self.findElements(By.tagName("td")).get(2);
    }

    private void initVenderId(){
        this.venderId = self.findElements(By.tagName("td")).get(3);
    }

    private void initAuthedTime(){
        this.authedTime = self.findElements(By.tagName("td")).get(4);
    }

    private void initCreateTime(){
        this.createTime = self.findElements(By.tagName("td")).get(5);
    }


    public String getVenderIdStr(){
        String text = this.venderId.getText();
        if(StringUtils.isBlank(text)){
            text = this.venderId.getAttribute("innerHTML");
        }
        return text;
    }
}
