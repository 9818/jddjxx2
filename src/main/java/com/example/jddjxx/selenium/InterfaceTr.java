package com.example.jddjxx.selenium;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

/**
 * 消息订阅行
 * <p>MsgTr</p>
 * description
 *
 * @author 邓峰峰
 * @date 2020/5/11 13:07
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class InterfaceTr extends SelfElement {
    private WebElement checkBox;
    /**
     * 感叹号
     */
    private WebElement mark;
    private WebElement interfaceName;
    private WebElement testStatus;
    private WebElement onlineStatus;
    private WebElement activation;
    private WebElement responseTempLate;
    private WebElement lastOpTime;

    public InterfaceTr() {
    }

    public InterfaceTr(WebElement self) {
        super.self = self;
        initCheckBox();
        initInterfaceName();
        initSubscribe();
        initTestStatus();
        initOnlineStatus();
        initOp();
        initLastOpTime();
    }

    public WebElement getCheckBox(){
        final WebElement td = self.findElements(By.tagName("td")).get(0);
        try {
            this.checkBox = td.findElement(By.tagName("input"));
        } catch (Exception ignored) {

        }
        return this.checkBox;
    }


    private void initCheckBox(){
        final WebElement td = self.findElements(By.tagName("td")).get(0);
        try {
            this.checkBox = td.findElement(By.tagName("input"));
        } catch (Exception ignored) {

        }
    }

    private void initInterfaceName(){
        this.interfaceName = self.findElements(By.tagName("td")).get(1);
    }

    private void initSubscribe(){
        this.activation = self.findElements(By.tagName("td")).get(2);
    }

    private void initTestStatus(){
        this.testStatus = self.findElements(By.tagName("td")).get(3);
    }

    private void initOnlineStatus(){
        this.onlineStatus = self.findElements(By.tagName("td")).get(4);
    }

    private void initOp(){
        final WebElement td = self.findElements(By.tagName("td")).get(5);
        final List<WebElement> spans = td.findElements(By.tagName("span"));
        this.activation = spans.get(0).findElement(By.tagName("a"));
        this.responseTempLate = spans.get(2).findElement(By.tagName("a"));
    }


    private void initLastOpTime(){
        final WebElement td = self.findElements(By.tagName("td")).get(0);
        try {
            this.checkBox = td.findElement(By.tagName("input"));
        } catch (Exception ignored) {

        }
    }

    @Override
    public void checked() {
        if(checkBox==null){

            return;
        }
        final String checkedStr = checkBox.getAttribute("checked");
        final boolean checked = BooleanUtils.toBoolean(checkedStr);
        if (!checked) {
            AutoOperate.doClick(checkBox);
        }
    }

    @Override
    public void unchecked() {
        if(checkBox==null){

            return;
        }
        final String checkedStr = checkBox.getAttribute("checked");
        final boolean checked = BooleanUtils.toBoolean(checkedStr);
        if (checked) {
            AutoOperate.doClick(checkBox);
        }
    }

    @Override
    public void activate() {
        if(!subscribed()){
            AutoOperate.doClick(activation);
        }
    }

    @Override
    public void deactivate() {
        if(subscribed()){
            AutoOperate.doClick(activation);
        }
    }

    /**
     *
     * @return
     */
    public String getInterfaceNameStr(){
        return interfaceName.getText();
    }


    /**
     * 是否已经订阅过消息
     * @return
     */
    public boolean subscribed(){
        String text = this.activation.getText();
        if(StringUtils.isBlank(text)){
            text = this.activation.getAttribute("innerHTML");
        }
        return "禁用".equals(text);
    }

}
