package com.example.jddjxx.selenium;

import org.openqa.selenium.*;
import org.openqa.selenium.support.events.AbstractWebDriverEventListener;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * <p>MyWebDriverEventListener</p>
 * description
 *
 * @author 邓峰峰
 * @date 2020/5/27 14:35
 */
public class MyWebDriverEventListener extends AbstractWebDriverEventListener {

    @Override
    public void afterClickOn(WebElement element, WebDriver driver) {
        try {

            System.out.println("出发了afterClickOn事件:"+element.getTagName());
            final Alert until = new WebDriverWait(driver, 10).until(ExpectedConditions
                    .alertIsPresent());
            until.accept();
//            driver.switchTo().alert().accept();
        } catch (NoAlertPresentException | UnhandledAlertException e) {
            // TODO: handle exception

            e.printStackTrace();
        }
    }
}
