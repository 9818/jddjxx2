package com.example.jddjxx.selenium;

/**
 * <p>Tr</p>
 * description
 *
 * @author 邓峰峰
 * @date 8/19/2020 3:53 PM
 */
public interface Tr {

    /**
     * 勾选复选框
     */
    void checked();

    /**
     * 取消复选框
     */
    void unchecked();

    /**
     * 激活/订阅/启用
     */
    void activate();

    /**
     * 取消激活/启用/订阅
     */
    void deactivate();
}
