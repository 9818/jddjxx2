package com.example.jddjxx.selenium;

import com.example.jddjxx.constant.Msg;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

/**
 * 消息订阅行
 * <p>MsgTr</p>
 * description
 *
 * @author 邓峰峰
 * @date 2020/5/11 13:07
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class MsgTr extends SelfElement {
    private WebElement checkBox;
    /**
     * 感叹号
     */
    private WebElement mark;
    private WebElement interfaceName;
    private WebElement testStatus;
    private WebElement onlineStatus;
    private WebElement subscribe;
    private WebElement configUrl;
    private WebElement responseTempLate;
    private WebElement lastOpTime;
    private Msg msg;


    public MsgTr() {
    }

    public MsgTr(WebElement self) {
        super.self = self;
        initCheckBox();
        initInterfaceName();
        initSubscribe();
        initTestStatus();
        initOnlineStatus();
        initOp();
        initLastOpTime();
        intiMsg();
    }

    @Override
    public void checked() {
        if(checkBox==null){
            initCheckBox();
            if(checkBox==null){
                return;
            }
        }

//        final String text = onlineStatus.getText();
//        final String span = onlineStatus.findElement(By.tagName("span")).getText();
        final String checkedStr = checkBox.getAttribute("checked");
        final boolean checked = BooleanUtils.toBoolean(checkedStr);
        if (!checked) {
            AutoOperate.doClick(checkBox);
        }
    }

    @Override
    public void unchecked() {
        if(checkBox==null){

            return;
        }
        final String checkedStr = checkBox.getAttribute("checked");
        final boolean checked = BooleanUtils.toBoolean(checkedStr);
        if (checked) {
            AutoOperate.doClick(checkBox);
        }
    }

    @Override
    public void activate() {
//        final Point location = subscribe.getLocation();
        if(!subscribed()){
            AutoOperate.doClick(subscribe);
        }

    }

    @Override
    public void deactivate() {
        if(subscribed()){
            AutoOperate.doClick(subscribe);
        }
    }

    private void intiMsg(){
        this.msg = Msg.valueBy(getInterfaceNameStr());
    }

    private void initCheckBox(){
        final WebElement td = self.findElements(By.tagName("td")).get(0);
        try {
            this.checkBox = td.findElement(By.tagName("input"));
        } catch (Exception ignored) {

        }
    }

    private void initInterfaceName(){
        this.interfaceName = self.findElements(By.tagName("td")).get(1);
    }

    private void initSubscribe(){
        this.subscribe = self.findElements(By.tagName("td")).get(2);
    }

    private void initTestStatus(){
        this.testStatus = self.findElements(By.tagName("td")).get(3);
    }

    private void initOnlineStatus(){
        this.onlineStatus = self.findElements(By.tagName("td")).get(4);
    }

    private void initOp(){
        final WebElement td = self.findElements(By.tagName("td")).get(5);
        final List<WebElement> spans = td.findElements(By.tagName("span"));
        this.subscribe = spans.get(0).findElement(By.tagName("a"));
        this.configUrl = spans.get(2).findElement(By.tagName("a"));
        this.responseTempLate = spans.get(3).findElement(By.tagName("a"));
    }


    private void initLastOpTime(){
        final WebElement td = self.findElements(By.tagName("td")).get(0);
        try {
            this.checkBox = td.findElement(By.tagName("input"));
        } catch (Exception ignored) {

        }
    }


    /**
     *
     * @return
     */
    public String getInterfaceNameStr(){
        return interfaceName.getText();
    }


    /**
     * 是否已经订阅过消息
     * @return
     */
    public boolean subscribed(){
        String text = this.subscribe.getText();
        if(StringUtils.isBlank(text)){
            text = this.subscribe.getAttribute("innerHTML");
        }
        return "取消订阅".equals(text);
    }

}
