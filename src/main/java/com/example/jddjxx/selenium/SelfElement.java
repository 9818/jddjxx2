package com.example.jddjxx.selenium;

import com.example.jddjxx.ChromeDriver;
import lombok.Data;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;

/**
 * <p>ScrollFun</p>
 * description
 *
 * @author 邓峰峰
 * @date 2020/5/12 16:01
 */
@Data
abstract class SelfElement implements Tr {

    protected WebElement self;


    /**
     * 移动滚动条到行位置可见
     */
    public void scrollToSelf() {
        if (self==null){
            return;
        }
        String js = "window.scrollTo(arguments[0],arguments[1])";
        final Point location = self.getLocation();
        if(location==null){
            return;
        }
        ChromeDriver.getInstance().executeScript(js, 0, location.getY()-200);
    }
}
