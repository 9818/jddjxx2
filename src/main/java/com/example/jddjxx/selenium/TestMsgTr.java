package com.example.jddjxx.selenium;

import com.example.jddjxx.constant.Msg;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

/**
 * 消息订阅行
 * <p>MsgTr</p>
 * description
 *
 * @author 邓峰峰
 * @date 2020/5/11 13:07
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class TestMsgTr extends SelfElement {
    private WebElement interfaceName;
    private WebElement testStatus;
    private WebElement onlineStatus;
    private WebElement send;
    private WebElement view;
    private Msg msg;

    public TestMsgTr() {
    }

    public TestMsgTr(WebElement self) {
        super.self = self;
        initInterfaceName();
        initTestStatus();
        initOnlineStatus();
        initOp();
        intiMsg();
    }

    @Override
    public void checked() {

    }

    @Override
    public void unchecked() {

    }

    @Override
    public void activate() {

    }

    @Override
    public void deactivate() {

    }

    private void intiMsg(){
        this.msg = Msg.valueBy(getInterfaceNameStr());
    }


    private void initInterfaceName(){
        this.interfaceName = self.findElements(By.tagName("td")).get(0);
    }


    private void initTestStatus(){
        this.testStatus = self.findElements(By.tagName("td")).get(1);
    }

    private void initOnlineStatus(){
        this.onlineStatus = self.findElements(By.tagName("td")).get(2);
    }

    private void initOp(){
        final WebElement td = self.findElements(By.tagName("td")).get(3);
        final List<WebElement> spans = td.findElements(By.tagName("span"));
        this.send = spans.get(0).findElement(By.tagName("a"));
        this.view = spans.get(1).findElement(By.tagName("a"));
    }



    /**
     *
     * @return
     */
    public String getInterfaceNameStr(){
        return interfaceName.getText();
    }


}
