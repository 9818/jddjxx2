package com.example.jddjxx.web.controller.vo;


/**
 * @author DFF
 */
public class Group {

    /**
     * 所有的企业,所有的消息修改配置服务地址组
     */
    public interface AllChangeConfigUrl {

    }

    /**
     * 修改配置服务地址组
     */
    public interface ChangeConfigUrl {

    }

    /**
     * 新企业配置服务地址组
     */
    public interface NewConfigUrl {

    }


    /**
     * 新企业配置服务地址组2
     */
    public interface NewConfigUrl2 {

    }

}
