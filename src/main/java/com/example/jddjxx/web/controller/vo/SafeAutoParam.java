package com.example.jddjxx.web.controller.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>SafeAutoParam</p>
 * description
 *
 * @author 邓峰峰
 * @date 2020/5/14 12:25
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class SafeAutoParam extends AutoParam {
    private boolean safe;

}
