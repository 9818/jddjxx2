package com.example.jddjxx.web.controller.vo;

import com.example.jddjxx.constant.Msg;
import com.example.jddjxx.constant.Scheme;
import com.example.jddjxx.web.entity.Ent;
import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Set;

/**
 * @author DFF
 */
@Data
@ToString
public class AutoParam {

    @NotEmpty(groups = {Group.ChangeConfigUrl.class,Group.NewConfigUrl.class},message = "请指定需要改变服务地址的消息")
    private Set<Msg> msgs;

    @NotEmpty(groups = {Group.NewConfigUrl2.class},message = "请指定需要改变服务地址的方案组")
    private List<Scheme> schemes;
    @NotBlank(groups = {Group.ChangeConfigUrl.class,Group.NewConfigUrl.class, Group.AllChangeConfigUrl.class,Group.NewConfigUrl2.class},message = "请输入正确的订单测试服务地址")
    private String testUrl;
    @NotBlank(groups = {Group.ChangeConfigUrl.class,Group.NewConfigUrl.class, Group.AllChangeConfigUrl.class,Group.NewConfigUrl2.class},message = "请输入正确的订单正式服务地址")
    private String prodUrl;
    @NotBlank(groups = {Group.ChangeConfigUrl.class,Group.NewConfigUrl.class, Group.AllChangeConfigUrl.class,Group.NewConfigUrl2.class},message = "请输入正确的商品测试服务地址")
    private String goodsTestUrl;
    @NotBlank(groups = {Group.ChangeConfigUrl.class,Group.NewConfigUrl.class, Group.AllChangeConfigUrl.class,Group.NewConfigUrl2.class},message = "请输入正确的商品正式服务地址")
    private String goodsProdUrl;
    @NotNull(groups = {Group.ChangeConfigUrl.class,Group.NewConfigUrl2.class},message = "请选择需要改变地址的应用")
    private List<Ent> enterprises;

    @NotEmpty(groups = {Group.NewConfigUrl.class},message = "请填写需要改变地址的应用")
    private List<String> enterpriseNames;

    @NotNull(groups = {Group.ChangeConfigUrl.class,Group.NewConfigUrl.class, Group.AllChangeConfigUrl.class,Group.NewConfigUrl2.class},message = "请选择是否是测试")
    private Boolean test;

    private boolean activeAllInterface;

    public AutoParam() {
    }




}
