package com.example.jddjxx.web.controller.vo;

import com.example.jddjxx.constant.Msg;
import com.example.jddjxx.web.entity.Ent;
import lombok.Data;

/**
 * @author DFF
 */
@Data
public class KeyValue {

    private String key;
    private String value;

    public KeyValue() {
    }

    public KeyValue(Ent enterprise) {
        this.key = enterprise.get_id().toString();
        this.value = enterprise.getName();
    }

    public KeyValue(Msg msg) {
        this.key = msg.name();
        this.value = msg.getTitle();
    }
}