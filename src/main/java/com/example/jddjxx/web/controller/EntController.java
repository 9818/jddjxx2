package com.example.jddjxx.web.controller;

import com.example.jddjxx.web.controller.vo.QnhResponse;
import com.example.jddjxx.web.entity.H2Ent;
import com.example.jddjxx.web.service.EntService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>EntController</p>
 * description
 *
 * @author 邓峰峰
 * @date 8/18/2020 4:19 PM
 */
@RequestMapping("ent")
@RestController
public class EntController {

    @Autowired
    EntService entService;

    @GetMapping("all")
    public List<H2Ent> findAll(){
        final List<H2Ent> all = entService.findAll();
        return all;
    }

    /**
     * 同步企业
     * @return
     */
    @PostMapping("sync")
    public QnhResponse<Void> syncEnt(){
        entService.syncEnt();
        return QnhResponse.success();
    }

}
