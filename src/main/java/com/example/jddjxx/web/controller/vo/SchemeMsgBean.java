package com.example.jddjxx.web.controller.vo;

import com.example.jddjxx.constant.Scheme;
import lombok.Data;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>Enterprise</p>
 * description
 *
 * @author 邓峰峰
 * @date 2020/5/19 17:07
 */
@Data
public class SchemeMsgBean {
    private Scheme scheme;
    private String title;
    private List<KeyValue> msgs;

    public SchemeMsgBean(Scheme scheme) {
        this.scheme = scheme;
        this.title = scheme.keyword();
        this.msgs = scheme.msgs().stream().map(KeyValue::new).collect(Collectors.toList());

    }

}
