package com.example.jddjxx.web.controller;

import com.example.jddjxx.web.entity.Ent;
import com.example.jddjxx.web.entity.EntMsg;
import com.example.jddjxx.web.service.EntMsgService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>EntController</p>
 * description
 *
 * @author 邓峰峰
 * @date 8/18/2020 4:19 PM
 */
@RequestMapping("ent/msg")
@RestController
public class EntMsgController {

    @Autowired
    EntMsgService entMsgService;

    @GetMapping("all")
    public List<EntMsg> findAll(Ent ent){
        final List<EntMsg> all = entMsgService.findAll(ent);
        return all;
    }
}
