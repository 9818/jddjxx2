package com.example.jddjxx.web.controller.vo;

import lombok.Data;

@Data
public class SchemeMsgsBean {

    SchemeMsgBean order3;
    SchemeMsgBean price;
    SchemeMsgBean goods;
    SchemeMsgBean afs;
    SchemeMsgBean sale;
    SchemeMsgBean vip;
    SchemeMsgBean money;
    SchemeMsgBean store;

}
