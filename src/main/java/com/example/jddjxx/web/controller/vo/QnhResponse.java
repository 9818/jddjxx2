package com.example.jddjxx.web.controller.vo;

import lombok.Data;

/**
 * <p>QnhResponse</p>
 * description
 *
 * @author 邓峰峰
 * @date 6/30/2020 2:50 PM
 */
@Data
public class QnhResponse<T> {

    /**
     * 是否成功
     */
    private boolean success;
    private String code;
    private String msg;
    private T data;

    private QnhResponse(){

    }

    public static <T> QnhResponse<T> success(T data){
        final QnhResponse<T> qnhResponse = new QnhResponse<T>();
        qnhResponse.setSuccess(true);
        qnhResponse.setCode("0");
        qnhResponse.setData(data);
        return qnhResponse;
    }


    public static <T> QnhResponse<T> success(){
        final QnhResponse<T> qnhResponse = new QnhResponse<T>();
        qnhResponse.setSuccess(true);
        qnhResponse.setCode("0");
        return qnhResponse;
    }

    public static <T> QnhResponse<T> fail(){
        final QnhResponse<T> qnhResponse = new QnhResponse<T>();
        qnhResponse.setSuccess(false);
        qnhResponse.setCode("500");
        return qnhResponse;
    }

    public static <T> QnhResponse<T> fail(String msg){
        final QnhResponse<T> qnhResponse = new QnhResponse<T>();
        qnhResponse.setSuccess(false);
        qnhResponse.setMsg(msg);
        qnhResponse.setCode("500");
        return qnhResponse;
    }

    public static <T> QnhResponse<T> fail(String code,String msg){
        final QnhResponse<T> qnhResponse = new QnhResponse<T>();
        qnhResponse.setSuccess(false);
        qnhResponse.setMsg(msg);
        qnhResponse.setCode(code);
        return qnhResponse;
    }


}
