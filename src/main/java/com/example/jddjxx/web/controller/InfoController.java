package com.example.jddjxx.web.controller;


import com.example.jddjxx.constant.Site;
import com.example.jddjxx.web.entity.H2Ent;
import com.example.jddjxx.web.service.DefaultSiteService;
import com.example.jddjxx.web.service.EntService;
import com.example.jddjxx.web.service.JsonFileSiteService;
import com.example.jddjxx.web.service.SiteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * @author DFF
 */
@RequestMapping("info")
@RestController
public class InfoController {

    @Autowired
    EntService entService;



    @GetMapping("enterprises")
    public List<H2Ent> getAllEnterprise(){
        final List<H2Ent> all = entService.findAll();
        return all;
    }

    @GetMapping("msgs")
    public Map<String, Site> getAllMsg(){
        Map<String, Site> map = null;
        SiteService siteService = new JsonFileSiteService();
        try {
            map = siteService.schemeMap();
        } catch (Exception exception) {
            siteService = new DefaultSiteService();
            map = siteService.schemeMap();
        }
        return map;
    }
}
