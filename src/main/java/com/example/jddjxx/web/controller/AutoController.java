package com.example.jddjxx.web.controller;

import com.example.jddjxx.constant.Msg;
import com.example.jddjxx.constant.Scheme;
import com.example.jddjxx.web.controller.vo.AutoParam;
import com.example.jddjxx.web.controller.vo.Group;
import com.example.jddjxx.web.controller.vo.QnhResponse;
import com.example.jddjxx.web.controller.vo.SafeAutoParam;
import com.example.jddjxx.web.entity.Ent;
import com.example.jddjxx.web.service.AutoService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>AutoConreoller</p>
 * description
 *
 * @author 邓峰峰
 * @date 2020/5/11 18:12
 */
@RequestMapping("auto")
@RestController
public class AutoController {

    @Autowired
    AutoService autoService;

    @PostMapping("initEnt")
    public QnhResponse<Void> initEnt(){
        autoService.initEnt();
        return QnhResponse.success();
    }


    @PostMapping("scheme/subscribeAndSetConfigUrl")
    public String schemeSubscribeAndSetConfigUrl(@RequestBody @Validated(Group.NewConfigUrl2.class) AutoParam autoParam){
        final Set<Msg> msgs = autoParam.getSchemes().stream().map(Scheme::msgs).flatMap(List::stream).collect(Collectors.toSet());
        autoParam.setMsgs(msgs);
        final List<Ent> enterprises = autoParam.getEnterprises();
        autoParam.setEnterprises(enterprises);
        final SafeAutoParam safeAutoParam = excludeEnterprise(autoParam);
        autoService.subscribeAndSetConfigUrl(safeAutoParam);
        return "OK";
    }



    /**
     * 指定一个企业订阅指定消息和配置地址
     * @param autoParam
     * @return
     */
    @PostMapping("subscribeAndSetConfigUrl")
    public String subscribeAndSetConfigUrl(@RequestBody @Validated(Group.ChangeConfigUrl.class) AutoParam autoParam){
        final SafeAutoParam safeAutoParam = excludeEnterprise(autoParam);
        autoService.subscribeAndSetConfigUrl(safeAutoParam);
        return "OK";
    }

    @PostMapping("new/subscribeAndSetConfigUrl")
    public String newSubscribeAndSetConfigUrl(@RequestBody @Validated(Group.NewConfigUrl.class) AutoParam autoParam){
        final SafeAutoParam safeAutoParam = excludeEnterprise(autoParam);
        autoService.newSubscribeAndSetConfigUrl(safeAutoParam);
        return "OK";
    }

    private SafeAutoParam excludeEnterprise(AutoParam autoParam){
        if (autoParam.getTest()){
            return excludeProdEnterprise(autoParam);
        }else{
            return excludeTestEnterprise(autoParam);
        }
    }


    /**
     * 排除测试企业
     * @param autoParam
     * @return
     */
    private SafeAutoParam excludeTestEnterprise(AutoParam autoParam ){
        final List<Ent> enterprises = autoParam.getEnterprises();
        final List<Ent> collect = enterprises.stream().filter(enterprise -> !enterprise.isTest()).collect(Collectors.toList());
        autoParam.setEnterprises(collect);
        SafeAutoParam safeAutoParam = new SafeAutoParam();
        BeanUtils.copyProperties(autoParam,safeAutoParam);
        safeAutoParam.setSafe(true);
        return safeAutoParam;
    }


    /**
     * 排除正式企业
     * @param autoParam
     * @return
     */
    private SafeAutoParam excludeProdEnterprise(AutoParam autoParam ){
        final List<Ent> enterprises = autoParam.getEnterprises();
        final List<Ent> collect = enterprises.stream().filter(Ent::isTest).collect(Collectors.toList());
        autoParam.setEnterprises(collect);
        SafeAutoParam safeAutoParam = new SafeAutoParam();
        BeanUtils.copyProperties(autoParam,safeAutoParam);
        safeAutoParam.setSafe(true);
        return safeAutoParam;
    }
}
