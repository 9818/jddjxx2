package com.example.jddjxx.web.repository;

import com.example.jddjxx.web.entity.EntMsg;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>EntRepository</p>
 * description
 *
 * @author 邓峰峰
 * @date 8/18/2020 3:27 PM
 */
@Repository
public interface EntMsgRepository extends MongoRepository<EntMsg, ObjectId> {

    @Query(value = "{'ent._id' : ?0 }")
    List<EntMsg> findByEntId(ObjectId id);
}
