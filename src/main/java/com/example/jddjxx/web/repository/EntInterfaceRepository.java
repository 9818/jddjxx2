package com.example.jddjxx.web.repository;

import com.example.jddjxx.web.entity.EntInterface;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * <p>EntRepository</p>
 * description
 *
 * @author 邓峰峰
 * @date 8/18/2020 3:27 PM
 */
@Repository
public interface EntInterfaceRepository extends MongoRepository<EntInterface, ObjectId> {
}
