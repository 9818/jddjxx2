package com.example.jddjxx.web.repository.h2;

import com.example.jddjxx.web.entity.H2Ent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * <p>H2EntRepository</p>
 * description
 *
 * @author 邓峰峰
 * @date 8/20/2020 2:34 PM
 */
@Repository
public interface H2EntRepository extends JpaRepository<H2Ent,Long> {
}
