package com.example.jddjxx.web.service;

import com.example.jddjxx.web.entity.Ent;
import com.example.jddjxx.web.entity.EntMsg;

import java.util.List;

/**
 * <p>EntMsgService</p>
 * description
 *
 * @author 邓峰峰
 * @date 8/18/2020 4:35 PM
 */
public interface EntMsgService {

    /**
     * findAll
     * @param ent
     * @return
     */
    List<EntMsg> findAll(Ent ent);
}
