package com.example.jddjxx.web.service;

import com.example.jddjxx.constant.Site;

import java.util.Map;

/**
 * <p>SiteService</p>
 * description
 *
 * @author 邓峰峰
 * @date 8/21/2020 11:51 AM
 */
public interface SiteService {

    /**
     * 获取map
     * @return
     */
    Map<String, Site> schemeMap();
}
