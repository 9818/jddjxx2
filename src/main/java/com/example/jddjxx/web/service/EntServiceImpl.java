package com.example.jddjxx.web.service;

import com.example.jddjxx.web.entity.H2Ent;
import com.example.jddjxx.web.repository.EntRepository;
import com.example.jddjxx.web.repository.h2.H2EntRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>EntServiceImpl</p>
 * description
 *
 * @author 邓峰峰
 * @date 8/18/2020 4:29 PM
 */
@Service
public class EntServiceImpl implements EntService {

    @Autowired
    EntRepository entRepository;

    @Autowired
    H2EntRepository h2EntRepository;

    @Override
    public List<H2Ent> findAll() {
        return h2EntRepository.findAll();
    }

    @Override
    public void syncEnt() {

    }
}
