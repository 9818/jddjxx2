package com.example.jddjxx.web.service;

import com.example.jddjxx.web.entity.Ent;
import com.example.jddjxx.web.entity.EntMsg;
import com.example.jddjxx.web.repository.EntMsgRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>EntMsgServiceImpl</p>
 * description
 *
 * @author 邓峰峰
 * @date 8/18/2020 4:36 PM
 */
@Service
public class EntMsgServiceImpl implements EntMsgService {

    @Autowired
    EntMsgRepository entMsgRepository;

    @Override
    public List<EntMsg> findAll(Ent ent) {
        if(ent==null){
            throw new IllegalArgumentException("企业不可为null");
        }
        return entMsgRepository.findByEntId(ent.get_id());
    }
}
