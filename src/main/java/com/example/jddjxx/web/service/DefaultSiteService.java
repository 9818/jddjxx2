package com.example.jddjxx.web.service;

import com.example.jddjxx.constant.Scheme;
import com.example.jddjxx.constant.Site;

import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * <p>DefaultSiteService</p>
 * description
 *
 * @author 邓峰峰
 * @date 8/21/2020 1:36 PM
 */
public class DefaultSiteService implements SiteService {
    @Override
    public Map<String, Site> schemeMap() {
        return  Arrays.stream(Scheme.values())
                .filter(scheme -> scheme.msgs()!=null)
                .map(Site::new)
                .collect(Collectors.toMap(Site::getScheme, Function.identity()));
    }
}
