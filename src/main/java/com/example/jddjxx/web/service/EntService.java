package com.example.jddjxx.web.service;

import com.example.jddjxx.web.entity.H2Ent;

import java.util.List;

/**
 * <p>EntService</p>
 * description
 *
 * @author 邓峰峰
 * @date 8/18/2020 4:28 PM
 */
public interface EntService {

    /**
     * findAll
     * @return
     */
    List<H2Ent> findAll();

    /**
     * 同步企业
     */
    void syncEnt();
}
