package com.example.jddjxx.web.service;

import com.example.jddjxx.constant.Msg;
import com.example.jddjxx.selenium.AutoOperate;
import com.example.jddjxx.web.controller.vo.SafeAutoParam;
import com.example.jddjxx.web.entity.Ent;
import com.example.jddjxx.web.entity.H2Ent;
import com.example.jddjxx.web.repository.h2.H2EntRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

/**
 * @author DFF
 */
@Service
@Slf4j
public class AutoService {

    @Value("${opendj.login.username}")
    private String username;

    @Value("${opendj.login.password}")
    private String password;

    @Autowired
    private H2EntRepository h2EntRepository;

    /**
     * 指定企业订阅指定消息并配置地址
     *
     * @param autoParam
     */
    public void subscribeAndSetConfigUrl(SafeAutoParam autoParam) {
        if (!autoParam.isSafe()){
            return;
        }
        AutoOperate autoOperate = new AutoOperate();
        autoOperate.open().login(username, password);
        final Set<Msg> msgs = autoParam.getMsgs();
        final List<Ent> enterprises = autoParam.getEnterprises();
        enterprises.forEach(enterprise -> {
            autoOperate
                    .toAuthedApp()
                    .clickScheme(enterprise)
                    .clickConfigUrl()
                    .subscribeAndReplaceConfigUrl(autoParam,enterprise)
                    .toTest(msgs)
                    .sendAll()
                    .refreshTestResult()
                    .iWantToOnline()
                    .accept()
                    .lastAccept()
                    .goBackEnterpriseListPage()
            ;
        });

        autoOperate.close();

    }


    /**
     *指定新企业订阅指定消息并配置地址
     * @param autoParam
     */
    public void newSubscribeAndSetConfigUrl(SafeAutoParam autoParam) {
        if (!autoParam.isSafe()){
            return;
        }

    }

    public void initEnt() {
        AutoOperate autoOperate = new AutoOperate();
        List<H2Ent> h2Ents =autoOperate
                .open()
                .login(username, password)
                .toAuthedApp()
                .getAllEnt();
        h2EntRepository.deleteAll();
        h2EntRepository.saveAll(h2Ents);
        autoOperate.close();


    }
}
