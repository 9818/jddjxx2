package com.example.jddjxx.web.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Objects;

/**
 * <p>Ent</p>
 * description
 *
 * @author 邓峰峰
 * @date 8/18/2020 3:28 PM
 */
@Entity(name = "t_ent")
public class H2Ent {

    @Id
    @GeneratedValue
    private Long id;

    private String name;

    private String vendorId;
    private String appId;
    private String appSecret;

    private String authedTime;
    private String createTime;

    private String trXpath;
    /**
     * 该企业方案接口信息的xpath
     */
    private String xpath;

    /**
     * 是否是测试企业
     */
    private boolean test;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getXpath() {
        return xpath;
    }

    public void setXpath(String xpath) {
        this.xpath = xpath;
    }

    public boolean isTest() {
        return test;
    }

    public void setTest(boolean test) {
        this.test = test;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        H2Ent ent = (H2Ent) o;
        return Objects.equals(vendorId, ent.vendorId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(vendorId);
    }

    public String getAuthedTime() {
        return authedTime;
    }

    public void setAuthedTime(String authedTime) {
        this.authedTime = authedTime;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getTrXpath() {
        return trXpath;
    }

    public void setTrXpath(String trXpath) {
        this.trXpath = trXpath;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getAppSecret() {
        return appSecret;
    }

    public void setAppSecret(String appSecret) {
        this.appSecret = appSecret;
    }
}
