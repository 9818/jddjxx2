package com.example.jddjxx.web.entity;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;

/**
 * <p>Msg</p>
 * description
 *
 * @author 邓峰峰
 * @date 8/18/2020 3:45 PM
 */
@Document("jddjxx_ent_msg")
public class EntMsg {
    @MongoId
    private ObjectId _id;

    /**
     * 消息名称
     */
    private String name;

    /**
     * 测试url
     */
    private String testUrl;

    /**
     * 生产环境的url
     */
    private String prodUrl;


    /**
     * 该msg对应的ent
     */
    private Ent ent;

    /**
     * 所在的行的xpath
     */
    private String trXpath;

    /**
     * 勾选框的xpath
     */
    private String checkboxXpath;

    /**
     * 订阅消息/取消订阅的xpath
     */
    private String subscribeXpath;

    /**
     * 配置地址的xpath
     */
    private String configUrlXpath;
}
