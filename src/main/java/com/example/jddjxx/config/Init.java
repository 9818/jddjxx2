package com.example.jddjxx.config;

import com.example.jddjxx.web.repository.EntRepository;
import com.example.jddjxx.web.repository.h2.H2EntRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * <p>Init</p>
 * description
 *
 * @author 邓峰峰
 * @date 8/18/2020 3:39 PM
 */
@Component
public class Init {

    @Autowired
    EntRepository entRepository;

    @Autowired
    H2EntRepository h2EntRepository;

//    @PostConstruct
//    public void initEnts(){
//        for (Enterprise value : Enterprise.values()) {
//            H2Ent h2Ent =new H2Ent();
//            h2Ent.setName(value.getChinese());
//            h2Ent.setVendorId(value.getVenderId());
//            h2Ent.setTest(value.test());
//            h2EntRepository.save(h2Ent);
//
//            Ent ent =new Ent();
//            ent.setName(value.getChinese());
//            ent.setVendorId(value.getVenderId());
//            ent.setTest(value.test());
//            entRepository.save(ent);
//        }
//    }
}
