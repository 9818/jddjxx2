package com.example.jddjxx.config;

import java.io.File;
import java.io.FileNotFoundException;

/**
 * <p>ConfigSite</p>
 * description
 *
 * @author 邓峰峰
 * @date 8/21/2020 1:14 PM
 */
public class ConfigSite {

    /**
     * 获取外部config文件夹下的site.json
     * @return
     * @throws FileNotFoundException
     */
    public static File getConfigSiteJsonFile() throws FileNotFoundException {
        final String userDir = System.getProperty("user.dir");
        final File file = new File(userDir + File.separator + "config" + File.separator + "site.json");
        if(file.exists()){
            return file;
        }
        throw new FileNotFoundException("config/site.json不存在");
    }
}
