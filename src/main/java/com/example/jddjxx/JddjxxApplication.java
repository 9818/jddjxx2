package com.example.jddjxx;

import com.beust.jcommander.Parameter;
import com.example.jddjxx.constant.Site;
import com.example.jddjxx.web.service.JsonFileSiteService;
import com.example.jddjxx.web.service.SiteService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;
import java.util.Map;

@SpringBootApplication
public class JddjxxApplication {
    @Parameter(names={"--driver.path", "-dp"},help = true,required =false ,description = "this is chromedriver`s path")
    private String driverPath;


    public static void main(String[] args) throws IOException {

        SpringApplication.run(JddjxxApplication.class, args);

        SiteService siteService = new JsonFileSiteService();
        final Map<String, Site> stringSiteMap = siteService.schemeMap();
//        final EventFiringWebDriver instance = ChromeDriver.getInstance();
//        System.out.println(instance);

    }

}

